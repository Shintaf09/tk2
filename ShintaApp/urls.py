from django.urls import path, include
from django.contrib import admin
from .views import *

app_name = 'ShintaApp'

urlpatterns = [
	path('', index, name="index"),
    path('login/', loginpage, name="loginpage"),
    path('logout/', logout, name="logout"),
    path('register/', register, name="register"),

   

]