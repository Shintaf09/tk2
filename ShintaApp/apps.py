from django.apps import AppConfig


class ShintaappConfig(AppConfig):
    name = 'ShintaApp'
