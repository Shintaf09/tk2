from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from .models import Account
from .forms import LoginForm, RegisterForm

# Create your views here.

# Create your views here.
def index (request):
	return render (request,"masigatau.html")
def register(request):
    if request.method == "POST":
        register_form = RegisterForm(request.POST)
        if register_form.is_valid():
            if (authenticate(username=request.POST['username'], email=request.POST['email'], password=request.POST['password'])) is None:
                user = User.objects.create_user(
                    request.POST['username'], request.POST['email'], request.POST['password'])
                user.save()
                auth_login(request, user)
                return redirect('/')
            else:
                return render(request,"masigatau.html")
    
def loginpage(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = User.objects.filter(username=username).exists()
        if user:
            is_authenticate = authenticate(
                username=username, password=password)
            if is_authenticate:
                auth_login(request, is_authenticate)
                return redirect('/')
            else:
                print('error_password')
        else:
            print('error_user')
    return render(request, 'masigatau.html')
    
@login_required
def logout(request):
    auth_logout(request)
    return redirect('/')