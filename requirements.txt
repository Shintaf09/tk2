coverage==4.5.4
Django==2.2.5
gunicorn==19.9.0
selenium==3.141.0
virtualenv==16.7.5
whitenoise==4.1.3
